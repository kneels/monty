[Monty Hall problem](https://en.wikipedia.org/wiki/Monty_Hall_problem) simulation in Python.

```
py -3 .\monty.py
```

```
Iterations:             100000
Elapsed time:           0.785 seconds
Wins without switching: 33315 (33.31%)
Wins with switching:    66662 (66.66%)
```