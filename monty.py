import random, time

def Play(switch):
	doors = {'A', 'B', 'C'}
	prizeDoor = random.choice(tuple(doors))
	playerChoice = random.choice(tuple(doors)) # Initial door pick
	
	goatDoors = doors.difference({prizeDoor, playerChoice}) # Doors that are not the prize door or the player's pick
	doorsReduced = doors.difference(random.choice(tuple(goatDoors))) # Host opens a door with a goat behind it

	if switch:
		playerChoice = list(doorsReduced.symmetric_difference(playerChoice))[0] # Player switches doors
	
	return prizeDoor == playerChoice

winsWithoutSwitch = 0
winsWithSwitch = 0
iterations = 100000
timerStart = time.perf_counter()

for i in range(0, iterations):
	if Play(False):
		winsWithoutSwitch += 1
	if Play(True):
		winsWithSwitch += 1

timerEnd = time.perf_counter()

print("Iterations:\t\t" + str(iterations))
print("Elapsed time:\t\t{:.3f} seconds".format(timerEnd - timerStart))
print("Wins without switching:\t" + str(winsWithoutSwitch) + " ({0:.2f}%)".format((winsWithoutSwitch/iterations)*100))
print("Wins with switching:\t" + str(winsWithSwitch) + " ({0:.2f}%)".format((winsWithSwitch/iterations)*100))